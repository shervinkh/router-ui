const Path = require('path')
const webpack = require('webpack')
const LessPluginAutoPrefix = require('less-plugin-autoprefix')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const extractLess = new ExtractTextPlugin({
  filename: (getPath) => getPath('css/[name]-[contenthash].css').replace('css/js', 'css'),
  allChunks: true,
})

function relativePath(file) {
  return Path.resolve(__dirname, file)
}

module.exports = {
  devtool: 'eval-source-map',

  entry: {
    polyfill: 'babel-polyfill',
    app: './src/index',
  },

  output: {
    path: relativePath('./dist'),
    publicPath: '/',
    filename: 'js/[name].[hash].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          relativePath('src'),
        ],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'stage-0', 'react'],
          },
        },
      },
      {
        test: /(\.less)$/,
        use: extractLess.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader' },
            {
              loader: 'less-loader',
              options: {
                sourceMap: true,
                plugins: [new LessPluginAutoPrefix()],
              },
            },
          ],
        }),
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg|gif)$/,
        use: {
          loader: 'url-loader',
          query: {
            publicPath: '/',
            limit: 2000,
            name: 'static/[hash].[ext]',
          },
        },
      },
    ],
  },

  devServer: {
    disableHostCheck: true,
  },

  plugins: [
    extractLess,
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'polyfill',
      minChunks: Infinity,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      chunks: ['app'],
      minChunks: (module) => module.context && module.context.indexOf('node_modules') !== -1,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'runtime',
      minChunks: Infinity,
    }),
    new HtmlWebpackPlugin({
      template: relativePath('index.ejs'),
      inject: 'body',
      chunksSortMode: 'manual',
      chunks: [
        'runtime',
        'polyfill',
        'vendor',
        'app',
      ],
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),
  ],
}
