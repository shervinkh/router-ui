const Path = require('path')
const webpack = require('webpack')
const LessPluginAutoPrefix = require('less-plugin-autoprefix')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const extractLess = new ExtractTextPlugin({
  filename: (getPath) => getPath('css/[name]-[contenthash].css').replace('css/js', 'css'),
  allChunks: true,
})

function relativePath(file) {
  return Path.resolve(__dirname, file)
}

module.exports = {
  devtool: 'source-map',

  entry: {
    polyfill: 'babel-polyfill',
    app: './src/index',
  },

  output: {
    path: relativePath('./dist'),
    filename: 'js/[name].[chunkhash].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          relativePath('src'),
        ],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'stage-0', 'react'],
          },
        },
      },
      {
        test: /(\.less)$/,
        use: extractLess.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader' },
            {
              loader: 'less-loader',
              options: {
                plugins: [new LessPluginAutoPrefix()],
              },
            },
          ],
        }),
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg|gif)$/,
        use: {
          loader: 'url-loader',
          query: {
            publicPath: '/',
            limit: 2000,
            name: 'static/[hash].[ext]',
          },
        },
      },
    ],
  },

  plugins: [
    extractLess,
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      chunks: ['app'],
      minChunks: (module) => module.context && module.context.indexOf('node_modules') !== -1,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'runtime',
      chunks: ['app', 'vendor'],
      minChunks: Infinity,
    }),
    new HtmlWebpackPlugin({
      template: relativePath('index.ejs'),
      inject: 'body',
      chunksSortMode: 'manual',
      chunks: [
        'polyfill',
        'runtime',
        'vendor',
        'app',
      ],
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      minimize: true,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
    }),
    new CompressionPlugin(),
  ],
}
