const socket = new WebSocket('ws://localhost:7123') // eslint-disable-line

let subscriptions = []
let open = false

socket.addEventListener('open', function (event) {
  subscriptions.forEach(callback => {
    open = true
    callback('open', true)
  })
})

socket.addEventListener('close', function (event) {
  subscriptions.forEach(callback => {
    open = false
    callback('open', false)
  })
})

socket.addEventListener('message', function (event) {
  subscriptions.forEach(callback => {
    callback('message', JSON.parse(event.data))
  })
})

export function subscribe(callback) {
  subscriptions.push(callback)
}

export function unsubscribe(callback) {
  subscriptions = subscriptions.filter(item => item !== callback)
}

export function send(data) {
  socket.send(data)
}

export function isOpen() {
  return open
}
