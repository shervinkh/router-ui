import React from 'react'
import { subscribe, unsubscribe, isOpen, send } from './socket'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      socketOpen: false,
      screenSaver: null,
      data: {},
      action: null,
    }
    this.handleSocketData = this.handleSocketData.bind(this)
    this.putScreenSaver = this.putScreenSaver.bind(this)
  }

  componentWillMount() {
    this.setState({ socketOpen: isOpen() })
    subscribe(this.handleSocketData)
    this.screenSaverId = setInterval(this.putScreenSaver, 10000)
  }

  componentWillUnmount() {
    unsubscribe(this.handleSocketData)
    clearInterval(this.screenSaverId)
  }

  handleSocketData(event, data) {
    if (event === 'open') this.setState({ socketOpen: data })
    else if (event === 'message') this.setState({ data })
  }

  putScreenSaver() {
    let color = ((1 << 24) * Math.random() | 0).toString(16)
    color = '#' + Array(6 - color.length + 1).join('0') + color
    this.setState({ screenSaver: color })
    setTimeout(() => this.setState({ screenSaver: null }), 1000)
  }

  generateDataTable() {
    const headers = ['WiFi', 'AP', 'Modem', 'VPN']
    const infos = ['Status', 'Traffic', 'Today', 'Month']

    return <table>
      <tbody>
        <tr>
          <th></th>
          {headers.map(item => <th key={item}>{item}</th>)}
        </tr>
        {infos.map(info =>
          <tr key={info}>
            <td>{info}</td>
            {headers.map(item => <td key={item}>{this.parseValue((this.state.data[item] || {})[info])}</td>)}
          </tr>
        )}
      </tbody>
    </table>
  }

  generateNetworkTable() {
    const dnsTypes = ['Global', 'Local']
    const routeTypes = ['IR', 'Globe']

    return <table>
      <tbody>
        <tr>
          {dnsTypes.map(item => <th key={item}>{item} DNS</th>)}
          {routeTypes.map(item => <th key={item}>{item}</th>)}
        </tr>
        <tr>
          {dnsTypes.map(item => <td key={item}>{this.parseValue((this.state.data.DNS || {})[item])}</td>)}
          {routeTypes.map(item => <td key={item}>{this.parseValue((this.state.data.Route || {})[item])}</td>)}
        </tr>
      </tbody>
    </table>
  }

  generateSystemTable() {
    const systemTypes = ['Uptime', 'CPU', 'Memory']
    return <table>
      <tbody>
        <tr>
          {systemTypes.map(item => <th key={item}>{item}</th>)}
        </tr>
        <tr>
          {systemTypes.map(item => <td key={item}>{this.parseValue((this.state.data.System || {})[item])}</td>)}
        </tr>
      </tbody>
    </table>
  }

  generateActions() {
    const actions = ['Stop VPN', 'Restart VPN', 'Restart Modem', 'Restart Router']

    return <table className='noborder'>
      <tbody>
        <tr>
          {actions.map(item => <td key={item}><button onClick={() => this.handleAction(item)}>{item}</button></td>)}
        </tr>
      </tbody>
    </table>
  }

  parseValue(value) {
    return typeof value === 'object'
      ? <span>&darr; {value.Down} <br/> &uarr; {value.Up}</span>
      : this.parseColor(value)
  }

  parseColor(value) {
    let color = 'black'
    if (value.toLowerCase().startsWith('fail')) color = 'red'
    else if (value.toLowerCase().startsWith('ok')) color = 'green'
    else if (value.indexOf('%') !== -1) {
      const val = parseFloat(value)
      if (val < 50) color = 'green'
      else if (val > 90) color = 'red'
    }
    return <span style={{ color }}>{value}</span>
  }

  renderApp() {
    return <div>
      <table>
        <tbody>
          <tr>
            <th>Home Router</th>
            <th>{new Date().toLocaleString('en-gb').replace(/(\d+)\/(\d+)\/(\d+), (\d+):(\d+):(\d+)/, '$3/$2/$1 $4:$5:$6')}</th>
          </tr>
        </tbody>
      </table>
      {this.state.socketOpen
        ? this.generateDataTable()
        : <div>Could not connect to backend</div>
      }
      {this.state.socketOpen &&
        this.generateNetworkTable()
      }
      {this.state.socketOpen &&
        this.generateSystemTable()
      }
      {this.state.socketOpen &&
        this.generateActions()
      }
    </div>
  }

  handleAction(action) {
    send(action)
    this.setState({ action })
    setTimeout(() => this.setState({ action: null }), 5000)
  }

  render() {
    return this.state.action
      ? <div className='screensaver action'>
        <div>Doing Action...</div>
        <br />
        <div>{this.state.action}</div>
      </div>
      : this.state.screenSaver
        ? <div className='screensaver' style={{ backgroundColor: this.state.screenSaver }} />
        : this.renderApp()
  }
}

export default App
